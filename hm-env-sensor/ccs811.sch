EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 9
Title "hm-env-sensor"
Date "2021-07-04"
Rev "Gen 3"
Comp "HiMinds"
Comment1 "https://www.himinds.com"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4700 3800 0    50   Input ~ 0
SCL
Text HLabel 4700 3700 0    50   Output ~ 0
SDA
$Comp
L Sensor_Gas:CCS811 U4
U 1 1 60FE85D1
P 5900 3800
F 0 "U4" H 5550 4350 50  0000 C CNN
F 1 "CCS811" H 5650 4250 50  0000 C CNN
F 2 "Package_LGA:AMS_LGA-10-1EP_2.7x4mm_P0.6mm" H 5900 3200 50  0001 C CNN
F 3 "http://ams.com/eng/Products/Environmental-Sensors/Air-Quality-Sensors/CCS811" H 5900 3600 50  0001 C CNN
F 4 "CCS811B-JOPD500" H 5900 3800 50  0001 C CNN "MPN"
	1    5900 3800
	1    0    0    -1  
$EndComp
Text HLabel 4700 4000 0    50   Input ~ 0
nRST
Text HLabel 4700 4100 0    50   Input ~ 0
nWAKE
Text HLabel 4700 3600 0    50   Output ~ 0
nINT
Wire Wire Line
	6300 3600 6400 3600
Wire Wire Line
	6400 3600 6400 3700
Wire Wire Line
	6400 3700 6300 3700
Text Notes 4550 4450 0    50   ~ 0
i2c address: 0x5A\n
$Comp
L power:GND #PWR021
U 1 1 60FEE41C
P 5900 4400
F 0 "#PWR021" H 5900 4150 50  0001 C CNN
F 1 "GND" H 5905 4227 50  0000 C CNN
F 2 "" H 5900 4400 50  0001 C CNN
F 3 "" H 5900 4400 50  0001 C CNN
	1    5900 4400
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:0.1UF-25V-5%(0603)-SparkFun_BME280_Breakout_v10-eagle-import-ESP32-DevKit-Lipo_Rev_A1-rescue C?
U 1 1 60FF3ED7
P 6200 3100
AR Path="/5EB88B9B/60FF3ED7" Ref="C?"  Part="1" 
AR Path="/5ED177A1/60FF3ED7" Ref="C?"  Part="1" 
AR Path="/60F82D18/60FF3ED7" Ref="C10"  Part="1" 
F 0 "C10" V 6300 3200 59  0000 L BNN
F 1 "4.7uF/50V/20%/Y5V/C0603" V 6100 2850 59  0000 L BNN
F 2 "OLIMEX_RLC-FP:C_0603_5MIL_DWS" H 6200 3100 50  0001 C CNN
F 3 "" H 6200 3100 50  0001 C CNN
	1    6200 3100
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:0.1UF-25V-5%(0603)-SparkFun_BME280_Breakout_v10-eagle-import-ESP32-DevKit-Lipo_Rev_A1-rescue C?
U 1 1 60FF3EDD
P 6500 3100
AR Path="/5EB88B9B/60FF3EDD" Ref="C?"  Part="1" 
AR Path="/5ED177A1/60FF3EDD" Ref="C?"  Part="1" 
AR Path="/60F82D18/60FF3EDD" Ref="C11"  Part="1" 
F 0 "C11" V 6600 3200 59  0000 L BNN
F 1 "100nF/50V/20%/Y5V/C0603" V 6700 2900 59  0000 L BNN
F 2 "OLIMEX_RLC-FP:C_0603_5MIL_DWS" H 6500 3100 50  0001 C CNN
F 3 "" H 6500 3100 50  0001 C CNN
	1    6500 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3200 6200 3300
Wire Wire Line
	6200 2850 6200 2900
$Comp
L hm-env-sensor-rescue:+3.3V-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #PWR?
U 1 1 60FF3EE7
P 6350 2800
AR Path="/60FF3EE7" Ref="#PWR?"  Part="1" 
AR Path="/5ED177A1/60FF3EE7" Ref="#PWR?"  Part="1" 
AR Path="/60F82D18/60FF3EE7" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 6350 2650 50  0001 C CNN
F 1 "+3.3V" V 6350 2900 50  0000 L CNN
F 2 "" H 6350 2800 60  0000 C CNN
F 3 "" H 6350 2800 60  0000 C CNN
	1    6350 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2800 6350 2850
Connection ~ 6350 2850
Wire Wire Line
	6350 2850 6200 2850
Wire Wire Line
	6200 2850 5900 2850
Wire Wire Line
	5900 2850 5900 3300
Connection ~ 6200 2850
Wire Wire Line
	6200 3300 6350 3300
$Comp
L power:GND #PWR023
U 1 1 60FF4CAC
P 6350 3350
F 0 "#PWR023" H 6350 3100 50  0001 C CNN
F 1 "GND" H 6355 3177 50  0000 C CNN
F 2 "" H 6350 3350 50  0001 C CNN
F 3 "" H 6350 3350 50  0001 C CNN
	1    6350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3350 6350 3300
Connection ~ 6350 3300
Wire Wire Line
	6500 3200 6500 3300
Wire Wire Line
	6350 3300 6500 3300
Wire Wire Line
	6500 2850 6500 2900
Wire Wire Line
	6350 2850 6500 2850
Wire Wire Line
	5900 2850 5350 2850
Connection ~ 5900 2850
$Comp
L hm-env-sensor-rescue:4.7KOHM-1_10W-1%(0603)-SparkFun_BME280_Breakout_v10-eagle-import-ESP32-DevKit-Lipo_Rev_A1-rescue R?
U 1 1 60FF87FC
P 5350 3100
AR Path="/5EBC2AFC/60FF87FC" Ref="R?"  Part="1" 
AR Path="/60FF87FC" Ref="R?"  Part="1" 
AR Path="/60F82D18/60FF87FC" Ref="R32"  Part="1" 
F 0 "R32" H 5200 3159 59  0000 L BNN
F 1 "4.7k/R0603" H 5200 2970 59  0000 L BNN
F 2 "OLIMEX_RLC-FP:R_0603_5MIL_DWS" H 5350 3100 50  0001 C CNN
F 3 "" H 5350 3100 50  0001 C CNN
	1    5350 3100
	0    -1   -1   0   
$EndComp
$Comp
L hm-env-sensor-rescue:4.7KOHM-1_10W-1%(0603)-SparkFun_BME280_Breakout_v10-eagle-import-ESP32-DevKit-Lipo_Rev_A1-rescue R?
U 1 1 60FF911C
P 5050 3100
AR Path="/5EBC2AFC/60FF911C" Ref="R?"  Part="1" 
AR Path="/60FF911C" Ref="R?"  Part="1" 
AR Path="/60F82D18/60FF911C" Ref="R31"  Part="1" 
F 0 "R31" H 4900 3159 59  0000 L BNN
F 1 "4.7k/R0603" H 4900 2970 59  0000 L BNN
F 2 "OLIMEX_RLC-FP:R_0603_5MIL_DWS" H 5050 3100 50  0001 C CNN
F 3 "" H 5050 3100 50  0001 C CNN
	1    5050 3100
	0    -1   -1   0   
$EndComp
$Comp
L hm-env-sensor-rescue:4.7KOHM-1_10W-1%(0603)-SparkFun_BME280_Breakout_v10-eagle-import-ESP32-DevKit-Lipo_Rev_A1-rescue R?
U 1 1 60FF9609
P 4750 3100
AR Path="/5EBC2AFC/60FF9609" Ref="R?"  Part="1" 
AR Path="/60FF9609" Ref="R?"  Part="1" 
AR Path="/60F82D18/60FF9609" Ref="R30"  Part="1" 
F 0 "R30" H 4600 3159 59  0000 L BNN
F 1 "4.7k/R0603" H 4600 2970 59  0000 L BNN
F 2 "OLIMEX_RLC-FP:R_0603_5MIL_DWS" H 4750 3100 50  0001 C CNN
F 3 "" H 4750 3100 50  0001 C CNN
	1    4750 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 3600 5350 3600
Wire Wire Line
	4700 3700 5500 3700
Wire Wire Line
	4700 3800 5500 3800
Wire Wire Line
	4700 4000 5050 4000
Wire Wire Line
	4750 2850 4750 2900
Wire Wire Line
	5050 2850 5050 2900
Connection ~ 5050 2850
Wire Wire Line
	5050 2850 4750 2850
Wire Wire Line
	5350 2850 5350 2900
Connection ~ 5350 2850
Wire Wire Line
	5350 2850 5050 2850
Wire Wire Line
	5350 3300 5350 3600
Connection ~ 5350 3600
Wire Wire Line
	5350 3600 5500 3600
Wire Wire Line
	5050 3300 5050 4000
Connection ~ 5050 4000
Wire Wire Line
	5050 4000 5500 4000
Wire Wire Line
	4750 3300 4750 4100
Wire Wire Line
	4700 4100 4750 4100
Connection ~ 4750 4100
Wire Wire Line
	4750 4100 5500 4100
Text Notes 4700 2250 0    50   ~ 0
ScioSense CCS811\n
NoConn ~ 6300 3900
$Comp
L hm-env-sensor-rescue:4.7KOHM-1_10W-1%(0603)-SparkFun_BME280_Breakout_v10-eagle-import-ESP32-DevKit-Lipo_Rev_A1-rescue R?
U 1 1 611FE6F3
P 5450 4350
AR Path="/5EBC2AFC/611FE6F3" Ref="R?"  Part="1" 
AR Path="/611FE6F3" Ref="R?"  Part="1" 
AR Path="/60F82D18/611FE6F3" Ref="R33"  Part="1" 
F 0 "R33" H 5300 4409 59  0000 L BNN
F 1 "4.7k/R0603" H 5300 4220 59  0000 L BNN
F 2 "OLIMEX_RLC-FP:R_0603_5MIL_DWS" H 5450 4350 50  0001 C CNN
F 3 "" H 5450 4350 50  0001 C CNN
	1    5450 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3500 5200 4350
Wire Wire Line
	5200 3500 5500 3500
Wire Wire Line
	5900 4300 5900 4350
Wire Wire Line
	5200 4350 5250 4350
Wire Wire Line
	5650 4350 5900 4350
Connection ~ 5900 4350
Wire Wire Line
	5900 4350 5900 4400
$EndSCHEMATC
