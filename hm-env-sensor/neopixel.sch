EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title "hm-env-sensor"
Date "2021-07-04"
Rev "Gen 3"
Comp "HiMinds"
Comment1 "https://www.himinds.com"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3900 4100 0    50   Input ~ 0
Data_in
$Comp
L LED:WS2812B D21
U 1 1 6119A81B
P 4800 4100
F 0 "D21" H 4950 3850 50  0000 L CNN
F 1 "WS2812B" H 4950 3750 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 4850 3800 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 4900 3725 50  0001 L TNN
	1    4800 4100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR017
U 1 1 611AD723
P 4800 3450
F 0 "#PWR017" H 4800 3300 50  0001 C CNN
F 1 "+5V" H 4815 3623 50  0000 C CNN
F 2 "" H 4800 3450 50  0001 C CNN
F 3 "" H 4800 3450 50  0001 C CNN
	1    4800 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 611ADBEC
P 4800 4500
F 0 "#PWR019" H 4800 4250 50  0001 C CNN
F 1 "GND" H 4805 4327 50  0000 C CNN
F 2 "" H 4800 4500 50  0001 C CNN
F 3 "" H 4800 4500 50  0001 C CNN
	1    4800 4500
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:0.1UF-25V-5%(0603)-SparkFun_BME280_Breakout_v10-eagle-import-ESP32-DevKit-Lipo_Rev_A1-rescue C?
U 1 1 611B23E3
P 5250 3800
AR Path="/5EB88B9B/611B23E3" Ref="C?"  Part="1" 
AR Path="/5ED177A1/611B23E3" Ref="C?"  Part="1" 
AR Path="/60F2C772/611B23E3" Ref="C7"  Part="1" 
F 0 "C7" V 5350 3900 59  0000 L BNN
F 1 "100nF/50V/20%/Y5V/C0603" H 5400 3800 59  0000 L BNN
F 2 "OLIMEX_RLC-FP:C_0603_5MIL_DWS" H 5250 3800 50  0001 C CNN
F 3 "" H 5250 3800 50  0001 C CNN
	1    5250 3800
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:D_Schottky-OLIMEX_Diodes-ESP32-DevKit-Lipo_Rev_A1-rescue D?
U 1 1 611B8130
P 4150 4100
AR Path="/611B8130" Ref="D?"  Part="1" 
AR Path="/5EEAAF9D/611B8130" Ref="D?"  Part="1" 
AR Path="/60F2C772/611B8130" Ref="D20"  Part="1" 
F 0 "D20" H 4150 4225 50  0000 C CNN
F 1 "1N5822/SS34/SMA" H 4000 4000 50  0000 C CNN
F 2 "OLIMEX_Diodes-FP:SMA-KA" H 4150 4232 60  0001 C CNN
F 3 "" H 4150 4100 60  0000 C CNN
	1    4150 4100
	1    0    0    1   
$EndComp
$Comp
L hm-env-sensor-rescue:4.7KOHM-1_10W-1%(0603)-SparkFun_BME280_Breakout_v10-eagle-import-ESP32-DevKit-Lipo_Rev_A1-rescue R?
U 1 1 611B9FB1
P 4400 3800
AR Path="/5EBC2AFC/611B9FB1" Ref="R?"  Part="1" 
AR Path="/611B9FB1" Ref="R?"  Part="1" 
AR Path="/60F2C772/611B9FB1" Ref="R29"  Part="1" 
F 0 "R29" H 4300 3650 59  0000 L BNN
F 1 "4.7k/R0603" H 4300 3900 59  0000 L BNN
F 2 "OLIMEX_RLC-FP:R_0603_5MIL_DWS" H 4400 3800 50  0001 C CNN
F 3 "" H 4400 3800 50  0001 C CNN
	1    4400 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4300 4100 4400 4100
Wire Wire Line
	4400 4000 4400 4100
Connection ~ 4400 4100
Wire Wire Line
	4400 4100 4500 4100
Wire Wire Line
	3900 4100 4000 4100
NoConn ~ 5100 4100
Wire Wire Line
	4800 4400 4800 4500
Wire Wire Line
	4800 3450 4800 3550
Wire Wire Line
	4400 3600 4400 3550
Wire Wire Line
	4400 3550 4800 3550
Connection ~ 4800 3550
Wire Wire Line
	4800 3550 4800 3800
$Comp
L power:GND #PWR020
U 1 1 611C13B1
P 5250 3950
F 0 "#PWR020" H 5250 3700 50  0001 C CNN
F 1 "GND" H 5255 3777 50  0000 C CNN
F 2 "" H 5250 3950 50  0001 C CNN
F 3 "" H 5250 3950 50  0001 C CNN
	1    5250 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3900 5250 3950
Wire Wire Line
	5250 3550 5250 3600
Wire Wire Line
	4800 3550 5250 3550
Text Notes 4100 3100 0    50   ~ 0
Neopixel 5x5mm
$EndSCHEMATC
