EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title "hm-env-sensor"
Date "2021-07-04"
Rev "Gen 3"
Comp "HiMinds"
Comment1 "https://www.himinds.com"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L hm-env-sensor-rescue:PWR_FLAG-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #FLG?
U 1 1 611CA05C
P 6550 2550
AR Path="/611CA05C" Ref="#FLG?"  Part="1" 
AR Path="/5EEAAF9D/611CA05C" Ref="#FLG02"  Part="1" 
F 0 "#FLG02" H 6550 2645 50  0001 C CNN
F 1 "PWR_FLAG" H 6550 2750 50  0000 C CNN
F 2 "" H 6550 2550 60  0000 C CNN
F 3 "" H 6550 2550 60  0000 C CNN
	1    6550 2550
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:GND-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #PWR?
U 1 1 5EEC5B4E
P 6550 2650
AR Path="/5EEC5B4E" Ref="#PWR?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5B4E" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 6550 2400 50  0001 C CNN
F 1 "GND" H 6550 2500 50  0000 C CNN
F 2 "" H 6550 2650 60  0000 C CNN
F 3 "" H 6550 2650 60  0000 C CNN
	1    6550 2650
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:PWR_FLAG-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #FLG?
U 1 1 5EEC5B54
P 4750 3650
AR Path="/5EEC5B54" Ref="#FLG?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5B54" Ref="#FLG01"  Part="1" 
F 0 "#FLG01" H 4750 3745 50  0001 C CNN
F 1 "PWR_FLAG" H 4750 3850 50  0000 C CNN
F 2 "" H 4750 3650 60  0000 C CNN
F 3 "" H 4750 3650 60  0000 C CNN
	1    4750 3650
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:+5V-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #PWR?
U 1 1 5EEC5B63
P 4350 3550
AR Path="/5EEC5B63" Ref="#PWR?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5B63" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 4350 3400 50  0001 C CNN
F 1 "+5V" H 4350 3690 50  0000 C CNN
F 2 "" H 4350 3550 60  0000 C CNN
F 3 "" H 4350 3550 60  0000 C CNN
	1    4350 3550
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:SY8009AAAC(SOT23-5)-OLIMEX_IC-ESP32-DevKit-Lipo_Rev_A1-rescue U?
U 1 1 5EEC5B7E
P 5350 3850
AR Path="/5EEC5B7E" Ref="U?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5B7E" Ref="U9"  Part="1" 
F 0 "U9" H 5240 4120 50  0000 C CNN
F 1 "SY8089AAAC(SOT23-5)" H 5350 3600 39  0000 C CNN
F 2 "OLIMEX_Regulators-FP:SOT-23-5" H 5380 4000 20  0001 C CNN
F 3 "" H 5350 3850 60  0000 C CNN
F 4 "Value 1" H 5350 3850 60  0001 C CNN "Fieldname 1"
F 5 "Value2" H 5350 3850 60  0001 C CNN "Fieldname2"
F 6 "Value3" H 5350 3850 60  0001 C CNN "Fieldname3"
	1    5350 3850
	1    0    0    -1  
$EndComp
Text Notes 5150 4350 0    51   ~ 0
Vout=0.6*(1+Ra/Rb)
$Comp
L hm-env-sensor-rescue:L-OLIMEX_RCL-ESP32-DevKit-Lipo_Rev_A1-rescue L?
U 1 1 5EEC5B88
P 6050 3750
AR Path="/5EEC5B88" Ref="L?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5B88" Ref="L1"  Part="1" 
F 0 "L1" H 6050 3949 50  0000 C CNN
F 1 "2.2uH/1.5A/DCR<0.1R/CD32" H 6050 3866 35  0000 C CNN
F 2 "OLIMEX_RLC-FP:CD32" H 6000 3750 60  0001 C CNN
F 3 "" H 6000 3750 60  0000 C CNN
F 4 "Value 1" H 6050 3750 60  0001 C CNN "Fieldname 1"
F 5 "Value2" H 6050 3750 60  0001 C CNN "Fieldname2"
F 6 "Value3" H 6050 3750 60  0001 C CNN "Fieldname3"
	1    6050 3750
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:R-OLIMEX_RCL-ESP32-DevKit-Lipo_Rev_A1-rescue R?
U 1 1 5EEC5B91
P 6100 3850
AR Path="/5EEC5B91" Ref="R?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5B91" Ref="R12"  Part="1" 
F 0 "R12" H 6280 3890 50  0000 C CNN
F 1 "4.99k/1%/R0603" H 6100 3775 39  0000 C CNN
F 2 "OLIMEX_RLC-FP:R_0603_5MIL_DWS" H 6100 3780 30  0001 C CNN
F 3 "" V 6100 3850 30  0000 C CNN
F 4 "Value 1" H 6100 3850 60  0001 C CNN "Fieldname 1"
F 5 "Value2" H 6100 3850 60  0001 C CNN "Fieldname2"
F 6 "Value3" H 6100 3850 60  0001 C CNN "Fieldname3"
	1    6100 3850
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:R-OLIMEX_RCL-ESP32-DevKit-Lipo_Rev_A1-rescue R?
U 1 1 5EEC5B9A
P 6100 4050
AR Path="/5EEC5B9A" Ref="R?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5B9A" Ref="R21"  Part="1" 
F 0 "R21" H 6280 4090 50  0000 C CNN
F 1 "1.1k/1%/R0603" H 6100 3975 39  0000 C CNN
F 2 "OLIMEX_RLC-FP:R_0603_5MIL_DWS" H 6100 3980 30  0001 C CNN
F 3 "" V 6100 4050 30  0000 C CNN
F 4 "Value 1" H 6100 4050 60  0001 C CNN "Fieldname 1"
F 5 "Value2" H 6100 4050 60  0001 C CNN "Fieldname2"
F 6 "Value3" H 6100 4050 60  0001 C CNN "Fieldname3"
	1    6100 4050
	1    0    0    -1  
$EndComp
Text Notes 6050 3890 0    51   ~ 0
Ra
Text Notes 6050 4090 0    51   ~ 0
Rb
$Comp
L hm-env-sensor-rescue:GND-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #PWR?
U 1 1 5EEC5BA2
P 6850 4550
AR Path="/5EEC5BA2" Ref="#PWR?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5BA2" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 6850 4300 50  0001 C CNN
F 1 "GND" H 6850 4410 50  0000 C CNN
F 2 "" H 6850 4550 60  0000 C CNN
F 3 "" H 6850 4550 60  0000 C CNN
	1    6850 4550
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:GND-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #PWR?
U 1 1 5EEC5BA8
P 6550 4550
AR Path="/5EEC5BA8" Ref="#PWR?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5BA8" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 6550 4300 50  0001 C CNN
F 1 "GND" H 6550 4410 50  0000 C CNN
F 2 "" H 6550 4550 60  0000 C CNN
F 3 "" H 6550 4550 60  0000 C CNN
	1    6550 4550
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:+3.3V-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #PWR?
U 1 1 5EEC5BAE
P 6850 3550
AR Path="/5EEC5BAE" Ref="#PWR?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5BAE" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 6850 3400 50  0001 C CNN
F 1 "+3.3V" H 6850 3700 50  0000 C CNN
F 2 "" H 6850 3550 60  0000 C CNN
F 3 "" H 6850 3550 60  0000 C CNN
	1    6850 3550
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:PWR_FLAG-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #FLG?
U 1 1 5EEC5BB4
P 6550 3550
AR Path="/5EEC5BB4" Ref="#FLG?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5BB4" Ref="#FLG03"  Part="1" 
F 0 "#FLG03" H 6550 3645 50  0001 C CNN
F 1 "PWR_FLAG" H 6550 3750 50  0000 C CNN
F 2 "" H 6550 3550 60  0000 C CNN
F 3 "" H 6550 3550 60  0000 C CNN
	1    6550 3550
	1    0    0    -1  
$EndComp
$Comp
L hm-env-sensor-rescue:GND-OLIMEX_Power-ESP32-DevKit-Lipo_Rev_A1-rescue #PWR?
U 1 1 5EEC5BBA
P 4950 4550
AR Path="/5EEC5BBA" Ref="#PWR?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5BBA" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 4950 4300 50  0001 C CNN
F 1 "GND" H 4950 4400 50  0000 C CNN
F 2 "" H 4950 4550 60  0000 C CNN
F 3 "" H 4950 4550 60  0000 C CNN
	1    4950 4550
	1    0    0    -1  
$EndComp
Text Notes 5250 3200 0    79   ~ 0
Power Supply
Wire Wire Line
	4350 3550 4350 3750
Wire Wire Line
	6550 2650 6550 2550
Wire Wire Line
	5050 3850 4950 3850
Wire Wire Line
	4950 3850 4950 3750
Connection ~ 4950 3750
Wire Wire Line
	5050 3950 4950 3950
Wire Wire Line
	4950 3950 4950 4550
Wire Wire Line
	5650 3750 5850 3750
Wire Wire Line
	5950 3850 5750 3850
Wire Wire Line
	5750 3850 5750 3950
Wire Wire Line
	5750 4050 5950 4050
Wire Wire Line
	5650 3950 5750 3950
Connection ~ 5750 3950
Wire Wire Line
	6550 4550 6550 4050
Wire Wire Line
	6550 4050 6250 4050
Wire Wire Line
	6250 3750 6550 3750
Wire Wire Line
	6850 3550 6850 3750
Wire Wire Line
	6550 3550 6550 3750
Connection ~ 6550 3750
Connection ~ 6850 3750
Wire Wire Line
	6850 4250 6850 4550
Wire Wire Line
	6250 3850 6550 3850
Wire Wire Line
	6550 3850 6550 3750
Connection ~ 4750 3750
Wire Wire Line
	4950 3750 5050 3750
Wire Wire Line
	5750 3950 5750 4050
Wire Wire Line
	6550 3750 6850 3750
Wire Wire Line
	6850 3750 6850 4050
Wire Wire Line
	4750 3750 4950 3750
$Comp
L hm-env-sensor-rescue:C-OLIMEX_RCL-ESP32-DevKit-Lipo_Rev_A1-rescue C?
U 1 1 5EEC5C4C
P 6850 4150
AR Path="/5EEC5C4C" Ref="C?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5C4C" Ref="C17"  Part="1" 
F 0 "C17" V 6775 4200 50  0000 L CNN
F 1 "47uF/6.3V/20%/X5R/C0805" V 6975 3800 40  0000 L CNN
F 2 "OLIMEX_RLC-FP:C_0805_5MIL_DWS" H 6850 4150 60  0001 C CNN
F 3 "" H 6850 4150 60  0000 C CNN
F 4 "Value 1" H 6850 4150 60  0001 C CNN "Fieldname 1"
F 5 "Value2" H 6850 4150 60  0001 C CNN "Fieldname2"
F 6 "Value3" H 6850 4150 60  0001 C CNN "Fieldname3"
	1    6850 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3750 4750 3650
$Comp
L hm-env-sensor-rescue:D_Schottky-OLIMEX_Diodes-ESP32-DevKit-Lipo_Rev_A1-rescue D?
U 1 1 5EEC5C56
P 4100 3750
AR Path="/5EEC5C56" Ref="D?"  Part="1" 
AR Path="/5EEAAF9D/5EEC5C56" Ref="D1"  Part="1" 
F 0 "D1" H 4100 3875 50  0000 C CNN
F 1 "1N5822/SS34/SMA" H 4050 3625 50  0000 C CNN
F 2 "OLIMEX_Diodes-FP:SMA-KA" H 4100 3882 60  0001 C CNN
F 3 "" H 4100 3750 60  0000 C CNN
	1    4100 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3950 3750 3750 3750
Text HLabel 3750 3750 0    50   Input ~ 0
+5V_USB
Wire Wire Line
	4350 3750 4750 3750
Wire Wire Line
	4250 3750 4350 3750
Connection ~ 4350 3750
$EndSCHEMATC
